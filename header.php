<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php wp_head();?>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?= get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <header>
      <div class="row">
        <div class="large-12 columns">
          <div class="top-bar">
            <div class="top-bar-left">
              <h1><?php bloginfo('name'); ?></h1>
            </div>
            <div class="top-bar-right"><?php wp_nav_menu( array('menu' => 'header-menu' )); ?></div>
          </div>
        </div>
      </div>
    </header>
