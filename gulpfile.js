var gulp = require('gulp');
var $    = require('gulp-load-plugins')();

var paths = {
  sass:[
    'scss/**/*.scss'
  ],
  scripts:[
    'node_modules/foundation-sites/dist/foundation.js'
    ,'js/**/*.js'
  ]
};

gulp.task('sass', function() {
  return gulp.src(paths.sass)
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('scripts', function() {
  return gulp.src(paths.scripts)
    .pipe($.concat('app.js'))
    .pipe(gulp.dest('dist'))
    .pipe($.uglify())
    .pipe($.rename('app.min.js'))
    .pipe(gulp.dest('dist'))
});

gulp.task('default', ['sass', 'scripts']);

gulp.task('watch', ['default'], function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.scripts, ['scripts']);
});
