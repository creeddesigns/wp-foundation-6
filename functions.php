<?php

/**
 * Proper way to enqueue scripts and styles
 */
function theme_scripts() {
	wp_enqueue_style( 'app.css', get_stylesheet_directory_uri() . '/dist/app.css' );
	wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'app.js', get_template_directory_uri() . '/dist/app.min.js', array('jquery'), '1.0.0', true );
}add_action( 'wp_enqueue_scripts', 'theme_scripts' );

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      // 'extra-menu' => __( 'Extra Menu' )
    )
  );
}add_action( 'init', 'register_my_menus' );
