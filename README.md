# Foundation for Sites Template

This is the basic starter project for [Foundation for Sites 6](http://foundation.zurb.com/sites) to use with wordpress. It includes a Sass compiler and a starter index.php file for you.

## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (0.10 or greater)
- [Git](https://git-scm.com/)

To manually set up the template, first download it with Git:

```bash
git clone https://github.com/zurb/foundation-sites-template projectname
```

Then open the folder in your command line, and install the needed dependencies:

```bash
cd projectname
npm install
```

Finally, run `gulp` or `gulp watch` to run the Sass compiler. Using `gulp watch` will re-run every time you save a Sass or JS file.
